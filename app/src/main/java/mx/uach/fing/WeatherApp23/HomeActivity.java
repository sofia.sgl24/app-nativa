package mx.uach.fing.WeatherApp23;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.util.logging.Logger;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        final Button btnCuu = findViewById(R.id.btnCuu);
        btnCuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Logger.getLogger("app ->").info("presione el botón");
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("CITY", "Chihuahua");
                statrActivity(intent);
            }
        });
    }

    public void goToMty(View v){
        Logger.getLogger("app ->").warning("presione mty");
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("CITY", "Monterrey");
        statrActivity(intent);
    }
}